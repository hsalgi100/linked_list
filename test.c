#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "linked_list.h"

void test_ll_append(){
    Node* list = ll_construct(37);
    ll_println(list);
    ll_append(list, 73);
    ll_println(list);
    ll_append(list, 8);
    ll_println(list);
}

void test_ll_get() {
    Node* list = ll_construct(37);
    ll_append(list, 73);
    ll_append(list, 8);

    printf("%d\n", ll_get(list, 0)->value);
    printf("%d\n", ll_get(list, 1)->value);
    printf("%d\n", ll_get(list, 2)->value); 
}

void test_11_append() {
    Node* list = ll_construct(37);
    ll_append(list, 73);
    ll_append(list, 8);
    ll_println(list);
}

void test_ll_prepend() {
    Node* list = ll_construct(37);
    list = ll_prepend(list, 5);
    ll_println(list);
}

void test_ll_insert_after() {
    Node* list = ll_construct(37);
    ll_insert_after(ll_get(list, 1), 69);
    ll_println(list);
}

void test_ll_shift_insert() {
    Node* list = ll_construct(0);
    ll_append(list, 2);
    ll_append(list, 3);
    ll_shift_insert(ll_get(list, 1), 1);
    ll_println(list);
}

void test_ll_delete_after() {
    Node* list = ll_construct(0);
    ll_append(list, -1);
    ll_append(list, 1);
    ll_println(list);
    ll_delete_next(list);
    ll_println(list);
}

void test_ll_shift_delete() {
    Node* list = ll_construct(0);
    ll_append(list, -1);
    ll_append(list, 1);
    ll_println(list);
    ll_shift_delete(ll_get(list, 1));
    ll_println(list);
}

void test_ll_remove_first_in_tail() {
    Node* list = ll_construct(0);
    ll_append(list, -1);
    ll_append(list, -2);
    ll_append(list, -1);
    ll_append(list, 1);
    ll_println(list);
    ll_remove_first_in_tail(list, -1);
    ll_println(list);
}

void test_ll_remove_last_in_tail() {
    Node* list = ll_construct(0);
    ll_append(list, -1);
    ll_append(list, 1);
    ll_append(list, -1);
    ll_println(list);
    ll_remove_last_in_tail(list, -1);
    ll_println(list);
}

void test_ll_find_first() {
    Node* list = ll_construct(0);
    ll_append(list, -1);
    ll_append(list, 1);
    ll_println(list);
    ll_println(ll_find_first(list, 2));
    ll_println(ll_find_first(list, -1));
}

void test_ll_find_last() {
    Node* list = ll_construct(0);
    ll_append(list, -1);
    ll_append(list, 1);
    ll_append(list, 2);
    ll_append(list, 1);
    ll_println(list);
    ll_println(ll_find_last(list, 1));
}

void test_ll_find_test_good() {
    Node* list = ll_construct(0);
    ll_append(list, 1);
    ll_append(list, 2);
    ll_append(list, 2);
    ll_append(list, 4);
    ll_println(list);
    Node* last = ll_find_last(list, 2);
    last->value = 3;
    ll_println(list);
}

void test_ll_construct() {
    Node* list = ll_construct(0);
    ll_append(list, -1);
    ll_append(list, 1);
    ll_println(list);
    ll_println(ll_find_first(list, 1));
    int a = ll_contains(list, 1);
    int b = ll_contains(list, 2);
    printf("%d\n", a);
    printf("%d\n", b);
}

void test_ll_dump() {
    Node* list = ll_construct(0);
    ll_append(list, -1);
    ll_append(list, 1);
    ll_dump(list);
}

int main() {
    // test_ll_remove_first_in_tail();
    // test_ll_dump();
    // test_ll_get();

    return 0;
}