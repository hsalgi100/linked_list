#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "linked_list.h"


// ====== PRINT ===============================================================

void ll_println(Node* this) {
    ll_print(this);
    printf("\n");
}

// ====== ALLOCATE ============================================================

Node* ll_construct(int value) {
    Node* result = (Node*)malloc(sizeof(Node));
    result->value = value;
    result->next = NULL;
    return result;
}

// ====== INSERT ==============================================================

Node* ll_append(Node* this, int value) {
    assert(this && "Cannot append to a null list");

    Node* last = ll_get_last(this);

    Node* new = ll_construct(value);
    last->next = new;
    return new;
}

/// Must capture result to free memory
Node* ll_prepend(Node* this, int value) {
    assert(this && "Cannot prepend to a null list");

    Node* head = ll_construct(value);
    head->next = this;

    return head;
}

/// Can't insert after last element
Node* ll_insert_after(Node* this, int value) {
    assert(this && "Cannot insert after null node");
    assert(this->next && "Cannot insert after last element, use append");

    // this -> next 
    Node* next = this->next;

    // this -> new -> next
    Node* new = ll_construct(value);
    this->next = new;
    new->next = next;

    return new;
}

Node* ll_shift_insert(Node* this, int value) {
    assert(this && "Cannot shift a null value over");
    assert(this->next && "Cannot shift if there is no next value");

    // this -> next
    // new -> this -> next
    ll_insert_after(this, this->value);
    this->value = value;

    return this;    
}

// ====== DELETE ==============================================================
/// Delete funcs care about the place but not the value

void ll_delete_next(Node* this){
    assert(this && "Cannot have a next if this is null");
    assert(this->next && "Cannot delete next if there is no next");

    // this -> next(d) -> nextnext
    // this -> nextnext, c may be null
    Node* next = this->next;
    this->next = next->next;
    free(next);
}

/// Invalidates this and next
/// - Invalidates this by changing value
/// - Invalidates next by freeing it
void ll_shift_delete(Node* this) {
    // A[a] -> B[b] -> C[c]
    Node* next = this->next;
    *this = *next;
    // A[b] -> C[c], B[b]

    free(next);
}

// ====== SEARCH ==============================================================

int ll_contains(Node* this, int value) {
    if (ll_find_first(this, value)) {
        return 1;
    }

    return 0;
}

// ====== FREE ================================================================

/// Free the whole list
void ll_free(Node* this) {
    assert(this && "cannot free a null list");

    Node* next = this->next;
    free(this);

    if (next) {
        ll_free(next);
    }
}
