.SUFFIXES:

llr:
	gcc linked_list_recursive.c -c -g -o linked_list_recursive.o
	gcc linked_list_common.c    -c -g -o linked_list_common.o
	gcc test_recursive.c        -c -g -o test_recursive.o
	gcc test_recursive.o linked_list_common.o linked_list_recursive.o -o linked_list
	./linked_list

ll:
	gcc linked_list_iter.c   -c -g -o linked_list_iter.o
	gcc linked_list_common.c -c -g -o linked_list_common.o
	gcc test.c               -c -g -o test.o
	gcc test.o linked_list_common.o linked_list_iter.o -o linked_list
	./linked_list

memcheck:
	valgrind --tool=memcheck --leak-check=full --show-leak-kinds=all ./linked_list

clean:
	rm -f *.o linked_list vgcore.*
