#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "linked_list.h"

void test_ll_get() {
    Node* list = ll_construct(37);
    ll_append(list, 73);
    ll_append(list, 8);

    printf("%d\n", ll_get(list, 0)->value);
    printf("%d\n", ll_get(list, 1)->value);
    printf("%d\n", ll_get(list, 2)->value); 
}

void test_ll_dump() {
    Node* list = ll_construct(0);
    ll_append(list, -1);
    ll_append(list, 1);
    ll_dump(list);
}

void test_ll_remove_first_in_tail() {
    Node* list = ll_construct(0);
    ll_append(list, -1);
    ll_append(list, -2);
    ll_append(list, -1);
    ll_append(list, 1);
    ll_println(list);
    ll_remove_first_in_tail(list, -1);
    ll_println(list);
}

void test_ll_remove_last_in_tail() {
    Node* list = ll_construct(0);
    ll_append(list, -1);
    ll_append(list, 1);
    ll_append(list, -1);
    ll_println(list);
    ll_remove_last_in_tail(list, -1);
    ll_println(list);
}

void test_ll_find_first() {
    Node* list = ll_construct(0);
    ll_append(list, -1);
    ll_append(list, 1);
    ll_println(list);
    ll_println(ll_find_first(list, 2));
    ll_println(ll_find_first(list, -1));
}

void test_ll_find_last() {
    Node* list = ll_construct(0);
    ll_append(list, -1);
    ll_append(list, 1);
    ll_append(list, 2);
    ll_append(list, 1);
    ll_println(list);
    ll_println(ll_find_last(list, 1));
}

void test_ll_find_test_good() {
    Node* list = ll_construct(0);
    ll_append(list, 1);
    ll_append(list, 2);
    ll_append(list, 2);
    ll_append(list, 4);
    ll_println(list);
    Node* last = ll_find_last(list, 2);
    last->value = 3;
    ll_println(list);
}

int main() {
    test_ll_find_last();
    test_ll_find_test_good();
    // test_ll_find_first();
    // test_ll_remove_last_in_tail();
    // test_ll_remove_first_in_tail();
    // test_ll_dump();
    // test_ll_get();

    return 0;
}
