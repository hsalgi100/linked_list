#include <stdio.h>
#include <assert.h>

#include "linked_list.h"


// ====== PRINT ===============================================================

void ll_print(Node* begin) {
    if (begin == NULL) {
        printf("[]");
        return;
    }

    Node* curr = begin;

    printf("[%d", curr->value);

    while (curr->next) {
        curr = curr->next;
        printf(", %d", curr->value);
    }

    printf("]");
}

void ll_dump(Node* begin) {
    Node* curr = begin;

    printf("%d, %lu\n", curr->value, curr->next);

    while (curr->next) {
        curr = curr->next;
        printf("%d, %lu\n", curr->value, curr->next);
    }
}

// ====== ACCESS ==============================================================

Node* ll_get(Node* this, int index) {
    assert(this && "Cannot index a null list");

    Node* curr = this;
    int curr_i = 0;

    while (curr_i < index) {
        assert(curr->next && "Index out of bounds");
        curr = curr->next;
        curr_i += 1;
    }

    assert(curr && "Index out of bounds");
    return curr;
}

Node* ll_get_last(Node* this) {
    assert(this && "Null list has no last element");

    Node* curr = this;

    while (curr->next) {
        curr = curr->next;
    }

    return curr;
}

// ====== DELETE ==============================================================
/// Remove funcs care about the value but not the place

/// Remove first instance of value after this
int ll_remove_first_in_tail(Node* this, int value) {
    if (this == NULL) {
        return 0;
    }

    Node* curr = this;

    while (curr->next) {
        if (curr->next->value == value) {
            ll_delete_next(curr);
            return 1;
        }
        curr = curr->next;
    }

    return 0;
}

/// Remove last instance of value after this
int ll_remove_last_in_tail(Node* this, int value) {
    Node* curr = this;
    Node* match_cache = NULL;

    while (curr->next) {
        if (curr->next->value == value) {
            match_cache = curr;
        }
        curr = curr->next;
    }

    if (match_cache) {
        ll_delete_next(match_cache);
        return 1;
    }

    return 0;
}

// ====== SEARCH ==============================================================

/// Returns null unless a value is found
Node* ll_find_first(Node* this, int value) {
    assert(this && "Cannot search null value");

    if (this->value == value) {
        return this;
    }

    Node* curr = this;

    while (curr->next) {
        if (curr->value == value) {
            return curr;
        }

        curr = curr->next;
    }

    if (curr->value == value) {
        return curr;
    }

    return NULL;
}

/// Returns null unless a value is found
Node* ll_find_last(Node* this, int value) {
    assert(this && "Cannot search null value");

    Node* curr = this;
    Node* match_cache = NULL;

    while (curr->next) {
        if (curr->value == value) {
            match_cache = curr;
        }
        curr = curr->next;
    }
    if (curr->value == value) {
        match_cache = curr;
    }

    return match_cache;
}

