#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "linked_list.h"


// ====== PRINT ===============================================================
void ll_print_(Node* curr) {
    printf(", %d", curr->value);
    if (curr->next) {
        ll_print_(curr->next);
    }
}

void ll_print(Node* begin) {
    printf("[");
    if (begin) {
        printf("%d", begin->value);
        if (begin->next) {
            ll_print_(begin->next);
        }
    }
    printf("]");
}

void ll_dump(Node* begin) {
    Node* curr = begin;

    if (curr == NULL) {
        return;
    }

    printf("%d, %lx\n", curr->value, curr->next);
    curr = curr -> next;
    ll_dump(curr);

}

// ====== ACCESS ==============================================================

Node* ll_get(Node* this, int index) {
    assert(this && "Cannot index a null list");

    if (index == 0) return this;
    return ll_get(this->next, index - 1);
}

Node* ll_get_last(Node* this) {
    assert(this && "Cannot get the last of a null list");

    if (this->next == NULL) return this;
    return ll_get_last(this->next);
}

// ====== DELETE ==============================================================
/// Remove first instance of value after this
int ll_remove_first_in_tail(Node* this, int value) {
    if (this == NULL) {
        return 0;
    }

    if (this->next->value == value) {
        ll_delete_next(this);
        return 1;
    }

    return ll_remove_first_in_tail(this->next, value);
}

typedef Node* NodeIter;

void ll_remove_last_in_tail_(NodeIter* match_cache, Node* this, int value) {
    // match_cache is a pointer to a pointer
    assert(this && "Function should not be called with null 'this'");

    if (this->next == NULL) {
        return;
    }
    
    if (this->next && this->next->value == value) {
        *match_cache = this;
    }

    ll_remove_last_in_tail_(match_cache, this->next, value);

}

/// Remove last instance of value after this
int ll_remove_last_in_tail(Node* this, int value) {
    assert(this && "Cannot have tail with null list");

    Node* match_cache = NULL;

    ll_remove_last_in_tail_(&match_cache, this, value);

    if (match_cache) {
        ll_delete_next(match_cache);
        return 1;
    }

    return 0;
}

// ====== SEARCH ==============================================================

/// Returns null unless a value is found
Node* ll_find_first(Node* this, int value) {
    assert(this && "Cannot search null value");

    if (this->value == value) {
        return this;
    }

    if (this->next) {
        ll_find_first(this->next, value);
    }

    else return NULL;
}

/// Returns null unless a value is found
void ll_find_last_(NodeIter* match_cache, Node* this, int value) {
    assert(this && "Function should not be called with null 'this'");

    if (this->next == NULL) {
        return;
    }
    
    if (this->value == value) {
        *match_cache = this;
    }

    ll_find_last_(match_cache, this->next, value);

}

Node* ll_find_last(Node* this, int value) {
    assert(this && "Cannot search null value");

    Node* match_cache = NULL;

    ll_find_last_(&match_cache, this, value);

    if (match_cache) {
        return match_cache;
    }

    else return NULL;
}
