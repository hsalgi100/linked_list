#ifndef LINKED_LIST_H
#define LINKED_LIST_H

typedef struct Node_ {
    int value; 
    struct Node_* next;
} Node;

void ll_print(Node*);
void ll_println(Node*);
void ll_dump(Node*);

Node* ll_construct(int);

Node* ll_get(Node*, int);
Node* ll_get_last(Node*);

Node* ll_append(Node*, int);
Node* ll_prepend(Node*, int);
Node* ll_insert_after(Node*, int);
Node* ll_shift_insert(Node*, int);

void ll_delete_next(Node*);
void ll_shift_delete(Node*);
int ll_remove_first_in_tail(Node*, int);
int ll_remove_last_in_tail(Node*, int);

Node* ll_find_first(Node*, int);
Node* ll_find_last(Node*, int);
int ll_contains(Node*, int);

void ll_free(Node*);

#endif /*LINKED_LIST_H*/